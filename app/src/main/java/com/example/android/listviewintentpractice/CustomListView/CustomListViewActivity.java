package com.example.android.listviewintentpractice.CustomListView;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.example.android.listviewintentpractice.R;

import java.util.ArrayList;

public class CustomListViewActivity extends AppCompatActivity {
    private ListView listView;
    private WeatherListViewAdapter weatherListViewAdapter;
    //An arrayList of Weather object to store many days of weather data
    private ArrayList<Weather> weathers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_list_view);
        //generate 30 days of hard code weather
        weathers = Weather.generateSampleWeatherData(30);
        //fill the adapter with the weather data
        weatherListViewAdapter = new WeatherListViewAdapter(this, weathers);
        //bind the listView with the ListView object here
        listView = (ListView) findViewById(R.id.listView_custom);
        //set the adapter to the listView
        listView.setAdapter(weatherListViewAdapter);



    }
}
