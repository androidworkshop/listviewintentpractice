package com.example.android.listviewintentpractice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.example.android.listviewintentpractice.CustomListView.CustomListViewActivity;

public class MainActivity extends AppCompatActivity {
    private Button btnToCustomListView;
    //Data source
    private String[] months = {"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"};
    private ListView listViewMain;
    //adapter: An Adapter object acts as a bridge between an AdapterView and the underlying data for that view.
    private ArrayAdapter simpleAdapter;
    //An AdapterView is a view whose children are determined by an Adapter.
    /**
     * onCreate is called when the activity is created.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //binding the layout in the xml file with the ListView object
        listViewMain = (ListView) findViewById(R.id.listView_main);
        /**
         * First param: "this", the current activity context
         * Second param: "android.R.layout.simple_list_item_1", resource ID for list layout row item
         * Third param: "months", the input array
         */
        simpleAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, months);
        listViewMain.setAdapter(simpleAdapter);

        listViewMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(MainActivity.this, i + "", Toast.LENGTH_SHORT).show();
            }
        });

        bindButton();
    }

    private void bindButton(){
        btnToCustomListView = (Button) findViewById(R.id.btnToCustomListView);
        btnToCustomListView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, CustomListViewActivity.class));
            }
        });
    }
}
