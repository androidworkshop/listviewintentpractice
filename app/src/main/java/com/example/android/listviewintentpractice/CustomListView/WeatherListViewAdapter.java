package com.example.android.listviewintentpractice.CustomListView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.android.listviewintentpractice.R;

import java.util.ArrayList;

/**
 * Created by SunnyChan on 15/10/2016.
 */

public class WeatherListViewAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Weather> datalist;

    public WeatherListViewAdapter(Context context, ArrayList<Weather> datalist) {
        this.context = context;
        this.datalist = datalist;
    }

    @Override
    public int getCount() {
        return datalist.size();
    }

    @Override
    public Object getItem(int i) {
        return datalist.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.layout_weather_item, viewGroup, false);
        }

        TextView tvDate = (TextView) convertView.findViewById(R.id.tvDate);
        TextView tvLow = (TextView) convertView.findViewById(R.id.tvLow);
        TextView tvHigh = (TextView) convertView.findViewById(R.id.tvHigh);
        TextView tvDescription = (TextView) convertView.findViewById(R.id.tvDescription);

        Weather weather = (Weather) getItem(i);
        tvDate.setText(weather.getDay());
        tvLow.setText(weather.getLow());
        tvHigh.setText(weather.getHigh());
        tvDescription.setText(weather.getWeatherDescription());


        return convertView;
    }
}
