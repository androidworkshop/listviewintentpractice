package com.example.android.listviewintentpractice.CustomListView;

import java.util.ArrayList;

/**
 * Created by SunnyChan on 15/10/2016.
 */

public class Weather {
    private String day;
    private String low;
    private String high;
    private String weatherDescription;

    public Weather() {
    }

    public Weather(String day, String low, String high, String weatherDescription) {
        this.day = day;
        this.low = low;
        this.high = high;
        this.weatherDescription = weatherDescription;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getLow() {
        return low;
    }

    public void setLow(String low) {
        this.low = low;
    }

    public String getHigh() {
        return high;
    }

    public void setHigh(String high) {
        this.high = high;
    }

    public String getWeatherDescription() {
        return weatherDescription;
    }

    public void setWeatherDescription(String weatherDescription) {
        this.weatherDescription = weatherDescription;
    }

    public static ArrayList<Weather> generateSampleWeatherData(int numberOfDay){
        ArrayList<Weather> weatherArrayList = new ArrayList<>();
        for (int i = 0; i<numberOfDay;i++ ){
            Weather weather = new Weather("Day "+i,"15","20","Sunny");
            weatherArrayList.add(weather);
        }
        return weatherArrayList;
    }
}
